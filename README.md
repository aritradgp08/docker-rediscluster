
Smoke Test to check if Redis Clusters are up :

This is a process to set up a disposable redis cluster in the case of failure of the sentinels.

SET UP:

redis_0 - master

redis_1 - slave of redis_0

sentinel_0

sentinel_1

sentinel_2

The testing configuration of the sentinels that have been set are:

down-after-milliseconds 1000
failover-timeout 1000
parallel-syncs 1

The redis-cli is set up using apt-get install redis-server

docker pull redis
docker pull joshula/redis-sentinel

Then the provision script provision_docker_redis_cluster.sh is ran.

In case of failure, remove_docker_redis_cluster.sh will run.

Some other operations that can test the functionability:

Attach to a sentinel : sudo docker attach sentinel_1

Pause/Unpause redis instances : 

sudo docker pause redis_0
sudo docker unpause redis_0